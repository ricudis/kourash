
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , http = require('http')
  , path = require('path')
  , fs = require('fs')
  , background = require('./background.js')
  , db = require('./db.js')
  , fb = require('facebook-node-sdk')
  , everyauth = require('everyauth')
  , renderer = require('./renderer')
  , malakia = require('./malakia.js');

var access_logfile = fs.createWriteStream('./access.log', {flags: 'a'});

var app = express();

app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'ejs');
  app.set('uri_prefix','http://localhost:3000/');
  app.use(express.favicon());
  app.use(express.logger({stream: access_logfile }));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.cookieParser());
  app.use(express.session({ secret: 'tomounikaitoxtapodi' }));
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function(){
  app.use(express.errorHandler());
});

app.get('/oua', function(req,res){
  return(res.render("error"));
});
  
app.get('/', routes.index);

app.get('/phrase', function(req, res){
  set=malakia.generate_new_set();
  req.session.currentset=set;
  res.send(set);
});

app.get('/spamit', function(req,res) { 

  if (!req.session || !req.session.auth || !req.session.auth.loggedIn) {
    req.session.redirectPath = '/spamit';
    return(res.redirect(everyauth.facebook.entryPath()));
  }
  
  if (!req.session.currentset) { 
    req.session.currentset=malakia.generate_new_set();
  }
  
  renderer.render_to_fb(req,res,req.session.currentset);
});

// setup everyauth 

everyauth.facebook
    .appId('280631208737396')
    .appSecret('77e67e78d35437fe4e3ae3ec8e69bb5b')
    .entryPath('/auth/facebook')
    .callbackPath('/auth/facebook/callback')
    .findUserById(function (id, callback) { 
        db.findUserById1(id,callback);
    })
    .findOrCreateUser( function (session, accessToken, accessTokExtra, fbUserMetadata) {
      var userPromise = this.Promise();
      db.findOrCreateUser({id:"facebook:"+fbUserMetadata.id,metadata:fbUserMetadata},function(err,user) {
        if (err) return userPromise.fail(err);
        if (user) {
          session.userobject=user;
          return userPromise.fulfill(user);
        }
      });
      return(userPromise);
     })
     .sendResponse(function (res,req) { 
         if (this.redirectPath()) { 
           this.redirect(res, this.redirectPath);
         } else if (req.session && req.session.redirectPath) {
            redirectPath=req.session.redirectPath;
            delete req.session.redirectPath;
            this.redirect(res,redirectPath);         
         } else {
           this.redirect(res, "/");
         }    
      });

app.use(everyauth.middleware());

http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});
