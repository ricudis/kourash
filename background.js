var fs = require('fs')

function get_random_bg(app) { 
        var bgpath='/images/backgrounds/'
	var files=fs.readdirSync( __dirname + '/public/' + bgpath );
	file = files[Math.floor(Math.random() * files.length)];
	
	fname = bgpath + '/' + file;
	
	return fname;
}

function get_random_font() { 
   var fonts=[
     '14px \"Lucida Grande\", Helvetica, Arial, sans-serif;',
     '14px \"Comic Sans MS\",\"Lucida Grande\", Helvetica, Arial, sans-serif;'
           ];
           
   return (fonts[Math.floor(Math.random() * fonts.length)]);
}
            
module.exports.get_random_bg = get_random_bg;
module.exports.get_random_font = get_random_font;