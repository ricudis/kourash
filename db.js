var express = require('express');

var databaseUrl = "penicuik.lala.gr:21073/terrorista"; // "username:password@example.com/mydb";
var collections = ["members","users","imagesposted"];
var db = require("mongojs").connect(databaseUrl, collections);

function findUserById(id) { 
  db.users.findOne({id:id},function(err,le_user) {
    return(err,le_user); 
  });
}

function findUserById1(id,callback) { 
  db.users.findOne({id:id},function(err,le_user) {
    if (err) return callback(err);
    callback(null,le_user);
  });
}

function findOrCreateUser(userdata,callback) { 
  db.users.findOne({id: userdata.id},function(err,le_user) {
    var user={id:userdata.id,metadata:userdata.metadata}; 
    if (!le_user || le_user.length==0) {
      console.log("New user : "+user);
      db.users.save(user);
    } else {
      db.users.update({id:userdata.id},user);
    }
    callback(err,user);
  });
}


function store_membership(req,res,organwsh,melos) {
  db.members.find({member: melos, organization: organwsh}, function(err, terror) {
    if (!terror || terror.length==0) { 
      db.members.save({member: melos, organization: organwsh});
      res.send({success:1});
    } else { 
      res.send({success:0,error:"είσαι ήδη μέλος, μαλάκα!"});
    }
  });
}

function log_to_db(req,photo_path,url) { 
  if (req.session && req.session.currentset && req.session.auth && req.session.auth.facebook && req.session.auth.facebook.userId) {
    var posted={user: req.session.auth.facebook.userId, set: req.session.currentset,photo_path: photo_path, url:url};
    db.imagesposted.save(posted);
    console.log("New image posted : "+posted);
  }
}
             
//module.exports.test = test
module.exports.findOrCreateUser = findOrCreateUser
module.exports.findUserById = findUserById
module.exports.findUserById1 = findUserById1
module.exports.log = log_to_db