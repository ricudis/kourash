function post_fb(req,res,photo_path,tolkien,message) { 
  var https = require('https'); //Https module of Node.js
  var fs = require('fs'); //FileSystem module of Node.js
  var FormData = require('form-data'); //Pretty multipart form maker.
  var form = new FormData(); //Create multipart form
  var db = require('./db.js');

  form.append('file', fs.createReadStream(photo_path)); //Put file
  form.append('message', message); //Put message
   
  //POST request options, notice 'path' has access_token parameter
  var options = {
      method: 'post',
      host: 'graph.facebook.com',
      path: '/me/photos?access_token='+tolkien,
      headers: form.getHeaders(),
  }
  
  //Do POST request, callback for response
  var request = https.request(options, function (result){
                         fs.unlinkSync(photo_path);
                         if (result.statusCode == "200" ) {
                                 result.on('data', function(d) {
                                        obj=JSON.parse(d);
                                        if (obj.id) {
                                                url="http://www.facebook.com/photo.php?fbid="+obj.id;
                                                db.log(req,photo_path,url);
                                                return(res.redirect(url));
                                        } else { 
                                                return(res.redirect("/"));
                                        }
                                 });     
                                 return(res.redirect("/"));
                         } else {
                                 console.log("Facebook post returned "+result.statusCode);
                                 return(res.render("error")); 
                         }
  });
                          
  //Binds form to request
  form.pipe(request);
                           
  //If anything goes wrong (request-wise not FB)
  request.on('error', function (error) {
     console.log("unknown error on post fb:"+error);
     return(res.render("error"));
  });
  
}

function do_render(req,res,set,url,callback) {
        var phantom=require('node-phantom');
        phantom.create(function(error,ph){
                if (error) { return callback(error); }
                        
                ph.createPage(function(err,page){
                        if (err) { return callback(err); }
                        page.set('viewportSize',{width:1024,height:768});
                        page.open(url,function(err,status){
                                if (err) { return callback(err); }
                                if (status=='success') { 
                                        id="photo";
                                        for(var i = 0; i < 2; i++) {
                                                id = id + Math.random() * Math.pow(10, 17) + Math.random() * Math.pow(10, 17) + Math.random() * Math.pow(10, 17) + Math.random() * Math.pow(10, 17);
                                        }
                                        page.render("/var/www/tmp/"+id+".png",function(err){
                                                if (err) { return callback(err); }
                                                        callback(null,id);
                                        });
                                };
                        });
                });
        });
}

function render_to_fb(req,res,set) {
        var url=encodeURI('http://localhost:3000/?phrase='+set.phrase+'&headline='+set.headline+'&background='+set.background+'&printable=1');
        do_render(req,res,set,url,function (err,id) { 
                if (err) {
                        console.log("From render callback: "+err);
                        return(res.render("error"));
                }
                var photo_path="/var/www/tmp/"+id+".png";
                var message=set.headline+" !! Μόνο στο http://penicuik.lala.gr/revolution/";
//                var photo_path="/tmp/gata.jpeg";
//                var message="Για ακομα περισσοτερες γατες!";
                
//                return(res.render("error"));
                
                if (req.session && req.session.auth && req.session.auth.facebook && req.session.auth.facebook.accessToken) { 
                        post_fb(req,res,photo_path,req.session.auth.facebook.accessToken,message);
                } else { 
                        console.log("No session data or auth while trying to post");
                        return(res.render("error"));
                }
        });
}

exports.render_to_fb = render_to_fb;        
