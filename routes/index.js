var  malakia = require('../malakia.js');
var  background = require('../background.js');

/*
 * GET home page.
 */

exports.index = function(req, res){
        var printable=0;
        
        if (!req.session || !req.session.currentset) { 
                req.session.currentset=malakia.generate_new_set();;
        }
                    
        if (req.query.printable) { 
                printable=1;
        }
        
        if (req.query.headline) { 
                req.session.currentset.headline=req.query.headline;
        }
        
        if (req.query.phrase) { 
                req.session.currentset.phrase=req.query.phrase;
        }
        
        if (req.query.background) { 
                req.session.currentset.background=req.query.background;
        }
        
        res.render('index', { set:req.session.currentset, printable:printable });
};
